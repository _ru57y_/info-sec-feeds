# Info-Sec-Feeds

This is a curated list of information security related RSS and atom feeds. 
Feel free to submit your suggestion based on the criteria below.


## Feed Selection

### What I look for in a feed:

High value concise information that aids in establishing an overall view of the current threat landscape
and/or staying to up-to-date with the latest vulnerabilites, exploits, tools, and methodologies.


### What I avoid when selecting feeds:
* Marketing and advertisements. 
* Generic advice geared for regular users.
* Feeds that supply nothing more than the article's title.

## Installation/Use

I currently use **newsboat** as my RSS reader.
The **urls** file can be directly imported to newsboat by dropping it into your **.newsboat/** directory.

*An opml file is not currently available.* 
You will have to manually import the urls if your reader uses this type of file
and does not have an option to import a list of urls.


